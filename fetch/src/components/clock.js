import React, { Component } from 'react';

class  Clock extends Component {

  constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }
    componentDidMount() {
        console.log("did mount called");
        this.timerID = setInterval(
            () => this.tick(),
                1000
        );
    }
    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        console.log("render is called");
        return (
            <div>
                <h2>clock</h2>
                <h3> {this.state.date.toLocaleTimeString()}</h3>
            </div>
        )
    }
}
export default Clock;