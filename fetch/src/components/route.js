import React from 'react';
import {Route} from 'react-router-dom';
import {Switch,BrowserRouter} from 'react-router-dom';

import Car from './car';
// import Clock from './clock';
// import Registered from './registered';
import Home from './home';
import CarDetail from './cardetails';
import FetchUsers from './fetchusers';


const Routes = () =>(
   <BrowserRouter>
   <Switch>
       {
           //AUTH Routes
       }
       <Route path={"/"} component={Home} exact/>
       {/* <Route path={"/clock"} component={Clock} />
       <Route path={"/registered"} component={Registered} /> */}
       <Route path = {"/fetchusers"} component={FetchUsers} />
       {/* <Route path = {"/car"} component={Car}/>
       <Route path = {"/cardetails"} component = {CarDetail}/> */}
   </Switch>
   </BrowserRouter>
)
export default Routes;