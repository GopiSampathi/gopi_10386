import React, { Component } from 'react';
import './App.css';
import Header from './component/header';
import Footer from './component/footer';
import Train from './component/train';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Train />
        <Footer />        
      </div>
    );
  }
}

export default App;
