import React, { Component } from 'react';
import '../train.css';
 

class Train extends Component {
  render() {
    return (
      <div>
      
       <div className="viewport">
            <div className="loginhead">
            <p>BOOK</p>
            <p><span>YOUR TICKET</span></p> 
            <center><img src={"https://www.irctc.co.in/nget/assets/images/rail_icon.png"} / ></center>
            <input type="text" placeholder="From*"></input><br></br>
            <input type="text" placeholder="To*"></input><br></br>
            <input type="text" placeholder="From*"></input>
       </div>
      </div>
      <div className="container">
      <div>
        <div>
        <img src={"https://tpc.googlesyndication.com/simgad/17969433507924548644"}/>
       </div> 
      </div>

       <div class="container" style={{width:"75%"}}> 
  <div class="row">
    <div class="card-deck">
      <div class="card">
        <img class="card-img-top" src="https://www.icicibank.com/managed-assets/images/offer-zone/imobile/welcome-offers-UPI-t.jpg" alt="Card image cap"/>
        <div class="card-block">
        <h6 class="card-title">Offer on first UPI payment through iMobile app</h6>
        <p class="exp-desktop">Expires on 28-02-2019</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="https://www.icicibank.com/managed-assets/images/offer-zone/senior-club-saving-acc/bookmyshow-offer-t.jpg" alt="Card image cap"/>
        <div class="card-block">
          <h6 class="card-title">BookMyShow Offer - Buy 2 tickets and get 2 tickets free</h6>
          <p class="exp-desktop">Expires on 31-03-2019</p>  
                  <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="https://www.icicibank.com/managed-assets/images/offer-zone/shopping/amazon-svd-offer-t.jpg" alt="Card image cap"/>
        <div class="card-block">
          <h6 class="card-title">Amazon Super Value Day Offer</h6>
          <p class="exp-desktop">Expires on 31-03-2019</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="https://www.icicibank.com/managed-assets/images/offer-zone/shopping/bigbasket-offer-t.jpg" alt="Card image cap"/>
        <div class="card-block">
          <h6 class="card-title">Bigbasket Offer - Get 20% Off</h6>
          <p class="exp-desktop">Expires on 28-02-2019</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
    </div>
  </div>
</div>
   </div>
  </div>
    );
  }
}

export default Train;
