import React, { Component } from 'react';
import './footer.css';


class Footer extends Component {
    state = {  }
    render() { 
        return (
            <footer className="footer">
<div className="container bottom_border">
<div className="row">
<div className=" col-sm-4 col-md col-sm-4  col-12 col">
<h5 className="headin5_amrc col_white_amrc pt2 footer-logo-top"><svg aria-hidden="true" class="svg-icon native iconLogoGlyphMd" width="32" height="37" viewBox="0 0 32 37"><g><path fill="#BCBBBB" d="M26 33v-9h4v13H0V24h4v9z"></path><path d="M23.1 25.99l.68-2.95-16.1-3.35L7 23l16.1 2.99zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm4.2-7.4L26 18.4l2.1-2.5L15.4 5.3l-2.1 2.5zM21.5 0l-2.7 2 9.9 13.3 2.7-2L21.5 0zM7 30h16v-3H7v3z" fill="#F48024"></path></g></svg> STACKOVERFLOW</h5>

<ul className="footer_ul_amrc">
<li><a href="">Questions</a></li>
<li><a href="">Jobs</a></li>
<li><a href="">Developers Jobs Directory</a></li>
<li><a href="">Salary Calculator</a></li>
<li><a href="">Help</a></li>
<li><a href="">Mobile</a></li>
<li><a href="">Disable Responsiveness</a></li>
</ul>


</div>


<div className=" col-sm-4 col-md  col-6 col">
<h5 className="headin5_amrc col_white_amrc pt2">PRODUCTS</h5>

<ul className="footer_ul_amrc">
<li><a href="">Teams</a></li>
<li><a href="">Talent</a></li>
<li><a href="">Hollow Man Montage</a></li>
<li><a href="">Engagement</a></li>
<li><a href="">Enterprise</a></li>

</ul>

</div>


<div className=" col-sm-4 col-md  col-6 col">
<h5 className="headin5_amrc col_white_amrc pt2">COMPANY</h5>

<ul className="footer_ul_amrc">
<li><a href="">About</a></li>
<li><a href="">Privacy</a></li>
<li><a href="">Work Here</a></li>
<li><a href="">Legal</a></li>
<li><a href="">Privacy and Policy</a></li>
<li><a href="">Contact Us</a></li>
</ul>

</div>


<div className=" col-sm-4 col-md  col-12 col">
<h5 className="headin5_amrc col_white_amrc pt2">STACK EXCHANGE NETWORK</h5>


<ul className="footer_ul2_amrc">
<li><a href="#"><i className=""></i> </a><p>Technology</p></li>
<li><a href="#"><i className=""></i> </a><p>Life/Arts</p></li>
<li><a href="#"><i className=""></i> </a><p>Culture / Recreation</p></li>
<li><a href="#"><i className=""></i> </a><p>Science</p></li>
<li><a href="#"><i className=""></i> </a><p>Other</p></li>
</ul>

</div>
</div>
</div>


<div className="container">
<ul className="foote_bottom_ul_amrc">
<li><a href="">Home</a></li>
<li><a href="">About</a></li>
<li><a href="">Services</a></li>
<li><a href="">Pricing</a></li>
<li><a href="">Blog</a></li>
<li><a href="">Contact</a></li>
</ul>

<p className="text-center">Copyright @2019 | Designed With by <a href="#">Innominds</a></p>

<ul className="social_footer_ul">
<li><a href=""><i className="fa fa-facebook-f"></i></a></li>
<li><a href=""><i className="fa fa-twitter"></i></a></li>
<li><a href=""><i className="fa fa-linkedin"></i></a></li>
<li><a href=""><i className="fa fa-instagram"></i></a></li>
</ul>

</div>

</footer>

          );
    }
}
 
export default Footer;