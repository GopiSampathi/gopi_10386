import React, { Component } from 'react';
import './footer.css';


class Footer extends Component {
    state = {  }
    render() { 
        return (
            <footer className="footer">
<div className="container bottom_border">
<div className="row">
<div className=" col-sm-4 col-md col-sm-4  col-12 col">
<h5 className="headin5_amrc col_white_amrc pt2 footer-logo-top"><svg aria-hidden="true" class="svg-icon native iconLogoGlyphMd" width="32" height="37" viewBox="0 0 32 37"><g><path fill="#BCBBBB" d="M26 33v-9h4v13H0V24h4v9z"></path><path d="" fill="#F48024"></path></g></svg> IRCTC</h5>

<ul className="footer_ul_amrc">
<li><a href="">General Information</a></li>
<li><a href="">Important Information</a></li>
<li><a href="">Agents</a></li>
<li><a href="">Enquiriesor</a></li>
<li><a href="">Help</a></li>
<li><a href="">Mobile</a></li>
<li><a href="">Disable Responsiveness</a></li>
</ul>


</div>


<div className=" col-sm-4 col-md  col-6 col">
<h5 className="headin5_amrc col_white_amrc pt2">PRODUCTS</h5>

<ul className="footer_ul_amrc">
<li><a href="">How To</a></li>
<li><a href="">IRCTC Official App</a></li>
<li><a href="">Advertise with us</a></li>
<li><a href="">Refund Rules</a></li>
<li><a href="">Enterprise</a></li>

</ul>

</div>


<div className=" col-sm-4 col-md  col-6 col">
<h5 className="headin5_amrc col_white_amrc pt2">COMPANY</h5>

<ul className="footer_ul_amrc">
<li><a href="">IRCTC eWallet</a></li>
<li><a href="">IRCTC Loyalty Program</a></li>
<li><a href="">About us</a></li>
<li><a href="">IRCTC Zone</a></li>
<li><a href="">Privacy and Policy</a></li>
<li><a href="">Contact Us</a></li>
</ul>

</div>


<div className=" col-sm-4 col-md  col-12 col">
<h5 className="headin5_amrc col_white_amrc pt2">ABOUT</h5>


<ul className="footer_ul2_amrc">
<li><a href="#"><i className=""></i> </a><p>For Newly Migrated Agents</p></li>
<li><a href="#"><i className=""></i> </a><p>Mobile Zone</p></li>
<li><a href="#"><i className=""></i> </a><p>IRCTC Prepaid</p></li>
<li><a href="#"><i className=""></i> </a><p>Integration Policy</p></li>
<li><a href="#"><i className=""></i> </a><p>Other</p></li>
</ul>

</div>
</div>
</div>


<div className="container">
<ul className="foote_bottom_ul_amrc">
<li><a href="">Home</a></li>
<li><a href="">About</a></li>
<li><a href="">Services</a></li>
<li><a href="">Pricing</a></li>
<li><a href="">Blog</a></li>
<li><a href="">Contact</a></li>
</ul>

<p className="text-center">Copyright @2019 | Designed With by <a href="#">Innominds</a></p>

<ul className="social_footer_ul">
<li><a href=""><i className="fa fa-facebook-f"></i></a></li>
<li><a href=""><i className="fa fa-twitter"></i></a></li>
<li><a href=""><i className="fa fa-linkedin"></i></a></li>
<li><a href=""><i className="fa fa-instagram"></i></a></li>
</ul>

</div>

</footer>

          );
    }
}
 
export default Footer;
