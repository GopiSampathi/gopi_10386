import React, { Component } from 'react';
import '../train.css';
import {Link} from 'react-router-dom';
 

class Train extends Component {
  render() {
    return (
      <div>
      
       <div className="viewport">
       <br/>
            <div className="loginhead">
            <p><h1>BOOK</h1></p>
            <p><span>YOUR TICKET</span></p> 
            <center><img src={"https://www.irctc.co.in/nget/assets/images/rail_icon.png"} /></center>
            <input class="from" type="text" placeholder="From*"></input><br></br>
            <br/>
            <input class="from" type="text" placeholder="To*"></input><br></br>
            <br/>
            <input autocomplete="off"  time datetime="1914-12-20 08:00" class="from" id="datetimepicker1" placeholder="Journey Date(dd-mm-yyyy)*"></input><br/>
            <br/>
            <select class="from" >
              <option value="volvo">All Classes</option>
              <option value="saab">AC First Class (1A)</option>
              <option value="mercedes">Exec. Chair Car (EC)</option>
              <option value="audi">AC 2 Tier (2A)</option>
              <option value="audi">First Class (FC)</option>
              <option value="audi">AC 3 Tier (3A)</option>
              <option value="audi">AC 3 Economy (3E)</option>
              <option value="audi">AC Chair car (CC)</option>
              <option value="audi">Sleeper (SL)</option>
              <option value="audi">Second Sitting (2S)</option>
            </select><br/>
            <br/>
           
              <div  className="form-check">
                <label className="form-check-label" for="check1">
                    <input type="checkbox" className="form-check-input" id="check1" name="option" value="MS Dhoni"  />
                    Flexiable With Date
                </label>
              </div>
              <div className="form-check">
                <label className="form-check-label" for="check2">
                    <input type="checkbox" className="form-check-input" id="check2" name="option" value="Dinesh Karthik" />
                    Divyaang or Jourbalist Concession
                </label>
              </div>
              <br/>
              <br/>
              <div class="FindTrain">
                <center><Link to="/body1">Find Trains</Link></center>
              </div>
              <br/>
              <br/>
              <div class="PNR">
                <center><p class="PNRStatus">PNR STATUS</p></center>
              </div>
              <div class="textheading">
                  <label >INDIAN RAILWAYS</label>
                  <span class="spanheading">safety</span>
                  <span class="spanheading">Security</span>
                  <span class="spanheading">Punctality</span>
              </div>              
       </div>
      </div>
      <div className="container">
      <div>
        <div>
        <img src={"https://tpc.googlesyndication.com/simgad/17969433507924548644"}/>
       </div> 
      </div>

       <div class="container" style={{width:"75%"}}>
       <h1>Have you not found the right one?
          Find a service suitable for you here.
        </h1>
        <br/>
        <div class="icons" >
        <i class="fa fa-plane" style={{fontSize:"62px",marginLeft:"15px"}}></i>
        <i class="fa fa-home" style={{fontSize:"62px",marginLeft:"255px"}}></i>
        <i class="fa fa-cutlery" style={{fontSize:"62px",marginLeft:"255px"}}></i> 
        <span><h4>Plane</h4></span> 
        <pre class="Holidays"><h4>Holidays</h4></pre>
        <pre class="Catering"><h4>Catering</h4></pre>       
        </div>
        <br/>
        <div>
        <img class="Advertise" src="https://tpc.googlesyndication.com/simgad/13366676322182902480"/>  
        </div>
        <br/>
        <br/>
       <h1 align="center">HOLIDAYS</h1> 
       <br/>
  <div class="row">
    <div class="card-deck">
      <div class="card">
        <img class="card-img-top" src="https://www.irctc.co.in/nget/assets/images/exterior.jpg" alt="Card image cap"/>
        <div class="card-block">
        <h2>Maharaja's Express</h2>
        <h6 class="card-title">Redefining Royalty, Luxury and Comfort, Maharajas’ express takes you 
        on a sojourn to the era of bygone stately splendour of princely states.
         Sylvan furnishings, elegant ambience and modern amenities are amalgamated for an
          “Experience Unsurpassed”.</h6>
        </div>
      </div>
      <br/>
      <br/>
      <div class="card">
        <img class="card-img-top" src="https://www.irctc.co.in/nget/assets/images/Thailand.jpg" alt="Card image cap"/>
        <div class="card-block">
        <h2>International Packages</h2>
          <h6 class="card-title">Best deals in International Holiday packages, handpicked by IRCTC, for Thailand, Dubai,
           Sri Lanka, Hong Kong, China, Macau, Bhutan, Nepal, U.K., Europe, USA, Australia etc.. The packages are inclusive
            of sightseeing, meals, visa charges and overseas medical insurance to </h6>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="https://www.irctc.co.in/nget/assets/images/Kashmir.jpg" alt="Card image cap"/>
        <div class="card-block">
        <h2>Domestic Air Packages</h2>
          <h6 class="card-title">Be it the spiritual devotee seeking blessings of Tirupati, Shirdi
           or Mata Vaishno Devi or the leisure traveller wanting
           to relish the Blue mountains of North East, Sand-dunes of Rajasthan, Hamlets of Ladakh, Wonders
           of Himalayas.</h6>         
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="https://www.irctc.co.in/nget/assets/images/Buddhist.jpg" alt="Card image cap"/>
        <div class="card-block">
        <h2>Tourist Trains</h2>
          <h6 class="card-title">Redefine luxury with Maharajas’ Express. Trace the footsteps of Buddha with Buddhist Circuit special train.
           Relive the ancient glory of steam engine by the heritage train- Steam Express.
           Travel through magnificent India with “Villages on Wheels”
           Bharat Darshan Train.</h6>          
        </div>
      </div>
    </div>
  </div>
</div>
   </div>
  </div>
    );
  }
}

export default Train;
