import React, { Component } from 'react';
import './body1.css';
import {Link} from 'react-router-dom';



class Body1 extends Component {
    render() {
        return(
            <div class="bodymain">
                <div class="topbody">
                <div class="textbox">
                    <a>Select Favourite Journey List</a>
                </div>
                <div class="colg1">
                    <label class="origin1">Origin</label>
                    <span class="spantag1">
                    <input class="selectstation" type="text" placeholder="Select Station"></input>
                    </span>
                </div>
                <a class="arrow">
                <i class="fa fa-exchange">
                </i>
                </a>
                <div class="colg2">
                    <label class="origin2">Destination</label>
                    <span class="spantag2">
                    <input class="SelectDestination" type="text" placeholder="Select Destination"></input>
                    </span>
                </div>
                <div class="colg2">
                    <label class="origin2">Journey Class</label>
                    <span class="spantag2">
                    <select class="JourneyClass" >
                        <option value="volvo">All Classes</option>
                        <option value="saab">AC First Class (1A)</option>
                        <option value="mercedes">Exec. Chair Car (EC)</option>
                        <option value="audi">AC 2 Tier (2A)</option>
                        <option value="audi">First Class (FC)</option>
                        <option value="audi">AC 3 Tier (3A)</option>
                        <option value="audi">AC 3 Economy (3E)</option>
                        <option value="audi">AC Chair car (CC)</option>
                        <option value="audi">Sleeper (SL)</option>
                        <option value="audi">Second Sitting (2S)</option>
                    </select>
                    </span>
                </div>
                <div class="colg2">
                    <label class="origin2">Journey Date</label>
                    <span class="spantag2">
                    <input class="SelectDate" type="date" placeholder="Select Date"></input>
                    </span>
                </div>
                <div class="colg2">
                    <label class="origin2">Journey Class</label>
                    <span class="spantag2">
                    <select>
                        <option value="volvo">Number Of Passengers</option>
                        <option value="saab">1</option>
                        <option value="mercedes">2</option>
                        <option value="audi">3</option>
                        <option value="audi">4</option>
                        <option value="audi">5</option>
                        <option value="audi">6</option>                        
                    </select>
                    </span>
                </div>
                <div class="colg2">
                    <label class="origin2">ModifySearch</label>
                    <span class="spantag2">
                    <div>
                    <Link to="/"><button class="Home" type="button" style={{background:"aqua"}}>Home</button></Link>
                    </div>
                    </span>
                </div>

                </div>
            </div>
        );
    }
}   
export default Body1;