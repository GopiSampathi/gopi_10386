import React , {Component} from 'react';


class FetchData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title : 'latest java title',
            body : 'latest body', 
        }
    }
    componentDidMount(){
        var data = {
            title: this.state.title,
            body: this.state.body
        };
        fetch('https://jsonplaceholder.typicode.com/posts',{
            method: 'POST',
            headers: {'Content-Type' :'application/json'},
            body: JSON.stringify(data)
        }).then((res) => res.json())
          .then((data) => console.log(data))
          .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div className="App">
            <h2>done</h2>
            </div>
         );
    }
}
 
export default FetchData;