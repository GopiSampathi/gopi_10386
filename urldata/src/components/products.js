import React, { Component } from 'react';
import './style.css';

class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoaded: false
        }
    }
    componentDidMount()  {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
        .then(res => res.json())
        .then(data => {
            this.setState({
                isLoaded: true,
                products: data
            })
        });
    }
    render() {
        var { isLoaded, products } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div classname="App">
                <div className ="Product">
                            <center><h2>Product Details</h2></center>
                      <center><table style={{textAlign:"center",border:"solid 2px white"}}>
                          <tr style={{backgroundColor:"#4CAF50",border:"solid 2px white"}}>
                              <th style={{border:"solid 1px white"}} >product_id</th>
                              <th style={{border:"solid 1px white"}}>Product Name</th>
                              <th style={{border:"solid 1px white"}}>price</th>
                              <th style={{border:"solid 1px white"}}>Quantity</th>
                              <th style={{border:"solid 1px white"}}>Type</th>
                          </tr>
                          
                            {products.map( items => (
                                <tr>
                                    <td style={{backgroundColor:"#ddd",border:"solid 2px white"}}>{items.product_id}</td> 
                                    <td style={{backgroundColor:"#f2f2f2",border:"solid 2px white"}}>{items.productname}</td>  
                                    <td style={{backgroundColor:"#f2f2f2",border:"solid 2px white"}}>{items.price} </td>
                                    <td style={{backgroundColor:"#f2f2f2",border:"solid 2px white"}}>{items.quantity}</td>
                                    <td style={{backgroundColor:"#f2f2f2",border:"solid 2px white"}}>{items.productType}</td>
                                    </tr>
                            ))}
                            
                      </table></center> 
                </div>
            </div>
        );
    }
}
export default Products;