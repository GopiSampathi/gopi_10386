import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import {createStore} from 'redux';
import configureStore from  './store/utilities/configureStore';


const storeInstance = configureStore()

ReactDOM.render(
<Provider store={storeInstance}>
  <App />
</Provider>, 
document.getElementById('root'));

   
serviceWorker.unregister();
