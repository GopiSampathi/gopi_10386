import React from 'react';
import {Route} from 'react-router-dom';
import {Switch,BrowserRouter} from 'react-router-dom';

import Car from './components/car';
import Clock from './components/clock';
import Registered from './components/registered';
import Home from './components/home';
import CarDetail from './components/cardetails';
import FetchUsers from './components/fetchusers';
import FetchData from './components/fetchdata';
import Employees from './components/employee';
import Products from './components/products';
import FetchAdd from './components/fetchadd';
import RandomUser from './components/randomusers';
import IncreaseQuntity from './components/increasequntity';
import DeleteQuntity from './components/decreasequntity';
import DecreaseQuntity from './components/decreasequntity';
import Counter from './components/counter';

const Routes = () =>(
   <BrowserRouter>
   <Switch>
       <Route path={"/"} component={Home} exact/>
       <Route path={"/clock"} component={Clock} />
       <Route path={"/registered"} component={Registered} />
       <Route path = {"/fetchusers"} component={FetchUsers} />
       <Route path = {"/fetchdata"} component = {FetchData} />
       <Route path = {"/employees"} component = {Employees} />
       <Route path = {"/products"} component = {Products} />
       <Route path = {"/fetchadd"} component = {FetchAdd} />
       <Route path = {"/car"} component={Car}/>
       <Route path = {"/cardetails"} component = {CarDetail}/>
       <Route path = {"/randomusers"} component = {RandomUser} />
       <Route path = {'/increaseQuantity'} component={IncreaseQuntity}/>
       <Route path={'/decreaseQuantity'} component={DecreaseQuntity}/>
       <Route path = {'/counter'} component = {Counter} />
   </Switch>
   </BrowserRouter>
)
export default Routes;