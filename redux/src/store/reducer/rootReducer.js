import { combineReducers } from 'redux';

import counterReducer from './counterReducer';
import newsReducer from './newsReducer';
// import productReducer from './productReducer';

const rootReducer = combineReducers({
  counterReducer,
  newsReducer
//   productReducer

});

export default rootReducer;
