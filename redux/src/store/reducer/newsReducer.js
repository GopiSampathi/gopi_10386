import * as allActions from '../actions/actionConstants';

const initialState = {
    news    : [],
    isLoaded   : false
}

export default function newsReducer(state = initialState,action){
    switch(action.type){
        case allActions.FETCH_NEWS:
        return action;

        case allActions.RECEIVE_NEWS:
        return{
            ...state,
            news: action.payload.articles,
            isloaded:true
        };
        default:
        return state;
    }
}