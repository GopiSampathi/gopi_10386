import request from 'superagent';
import * as allActions from '../actions/actionConstants';
import * as newsActions from '../actions/newsActions';


const newsService = (store) => next => action => {
    next(action)
    switch(action.type){
        case allActions.FETCH_NEWS:
        console.log("news service");
        request.get('http://13.229.176.226:8001/api/news/recent')
        .then(res =>{
            console.log("susses");
            const data =JSON.parse(res.text);
            console.log(data);
            next(newsActions.receiveNews(data));
        })
        .catch(err => {
            console.log("service failure");
            next({type:'FETCH_NEWS_DATA_ERROR',err});
        });
        break;

        default:
        break;
    }
}
export default newsService;