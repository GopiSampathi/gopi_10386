import React, { Component } from 'react';
import './style.css';

class IncreaseQuntity extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            productname : '',
            quantity :''
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("Product Id: "+this.state.productname);
        console.log("quantity: "+this.state.quantity);
        const data = {
            productname:this.state.productname,
            quantity:this.state.quantity
        }
        fetch(" http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/increaseQuantity",{
            method : 'PUT',
            headers :{'content-Type' : 'application/json'},
            body : JSON.stringify(data)
        })
        .then((res) => res.json)
        .then((data) => console.log(data))
        .then((data) => alert("Sussesfully Added"))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>  
                <form onSubmit={this.handleSubmit}>
                    Enter Product Name: <br/>
                    <input type="text" name="productname" value ={this.state.productname} placeholder="Enter Product Id" onChange={this.handleChange}/><br/>
                    Enter Product Quantity: <br/>
                    <input type="text" name="quantity" value ={this.state.quantity} placeholder="Enter Product Quantity" onChange={this.handleChange}/><br/>

                    <input type="submit" value= "submit"/>
                </form>
            </div>
         );
    }
}
 
export default IncreaseQuntity;