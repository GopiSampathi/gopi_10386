import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';


class News extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        this.props.newsActions.fetchNews();
    }
    render(){
        return(
            <React.Fragment>
                {
                    (this.props.newsItems)
                    ?<div>
                        {this.props.newsItems.map(item => (
                            <div key = {item._id}>
                            <h4>{item.heading}</h4>
                            <h4>{item.brief}</h4>
                            <h4>{item.body}</h4>
                            <h4>{item.timestamp}</h4>
                            <h4>{item.tags}</h4>
                            <h4>{item.comments}</h4>
                            </div>
                        ))}
                    </div>
                    : <div>
                        Loading...
                    </div>
                }
            </React.Fragment>
        );
    }
 }
 function mapStateToProps(state){
     return{
         newsItems: state.newsReducer.news,
     };
 }
 function mapDispatchToProps(dispatch){
     return{
         newsActions:bindActionCreators(newsActions , dispatch),
     };
 }
export default connect(mapStateToProps , mapDispatchToProps)(News);
