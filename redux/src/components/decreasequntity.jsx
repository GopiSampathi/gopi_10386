import React, { Component } from 'react';
import './style.css';

class DecreaseQuntity extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            productname : ''
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("Product Id: "+this.state.productname);
        const data = {
            productname:this.state.productname,
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/delProducts",{
            method : 'DELETE',
            headers :{'content-Type' : 'application/json'},
            body : JSON.stringify(data)
        })
        .then((res) => res.json)
        .then((data) => alert ("Deleted Successfully..!"))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>  
                <form onSubmit={this.handleSubmit}>
                    Enter Product Name: <br/>
                    <input type="text" name="productname" value ={this.state.productname} placeholder="Enter Product Name" onChange={this.handleChange}/><br/>

                    <input type="submit" value= "submit"/>
                </form>
            </div>
         );
    }
}
 
export default DecreaseQuntity;