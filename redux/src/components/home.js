import React,{Component} from 'react';
import {Link} from 'react-router-dom'


class Home extends Component{
    constructor(props){
        super(props);
        this.state={}
    }
    render(){
        return(            
        <div>
            <a href='/clock'>Clock</a><br/>
            <a href='/registered'>Registered</a><br/>
            <a href='/car'>Car</a>
            <br/>
            <Link to = '/employees'>employees</Link><br></br>
            <Link to = '/products'>Products</Link>
            <br/>
            <Link to = '/fetchadd'>FetchAdd</Link><br/>
            <Link to = '/randomusers'>RandomUsers</Link> 
            <Link to = '/increaseQuantity'>IncreaseQuntity</Link><br/>
            <Link to = '/decreaseQuantity'>Delete Products</Link><br/>
            <Link to = '/counter'>Counter</Link>                      
        </div>
        );
    }
}
export default Home;