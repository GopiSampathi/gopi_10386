import React, { Component } from 'react';

class Employees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoaded: false
        }
    }
    componentDidMount()  {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayEmployee')
        .then(res => res.json())
        .then(data => {
            this.setState({
                isLoaded: true,
                employees: data
            })
        });
    }
    render() {
        var { isLoaded, employees } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div classname="App">
                
                {/* <div class="container">
                <h2>Employees Details</h2>
                    <table class="table">
                    <tr>
                    {employees.map(employee => (
                        <li key="{employee.id}">
                        <tr>
                            <th>EmployeeId:{employee.employeeId}</th><br/>
                            <th>Name: {employee.employeeName}</th><br/>                                                        
                            <th>Role: {employee.role}</th><br/>                            
                        </tr>
                        </li>
                    ))}
                   </tr>
                    </table>
                </div> */}
                <div className ="Product">
                            <center><h2>Employee Details</h2></center>
                      <center><table style={{textAlign:"center",border:"solid 2px white"}}>
                          <tr style={{backgroundColor:"#4CAF50",border:"solid 2px white"}}>
                              <th style={{border:"solid 1px white"}} >Employee_id</th>
                              <th style={{border:"solid 1px white"}}>Employee Name</th>
                              <th style={{border:"solid 1px white"}}>Role</th>                             
                          </tr>
                          
                            {employees.map( items => (
                                <tr>
                                    <td style={{backgroundColor:"#ddd",border:"solid 2px white"}}>{items.employeeId}</td> 
                                    <td style={{backgroundColor:"#f2f2f2",border:"solid 2px white"}}>{items.employeeName}</td>  
                                    <td style={{backgroundColor:"#f2f2f2",border:"solid 2px white"}}>{items.role} </td>                                    
                                    </tr>
                            ))}
                            
                      </table></center> 
                </div>
            </div>
        );
    }
}
export default Employees;