import React, { Component } from 'react'


class Registered extends Component {
    constructor(props) {
        super(props);
        this.state = {
        username: " ",
            fav : " "
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            // username:event.target.value
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.username + ' ' + 'You have selected '+this.state.fav);
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <h3>Registration Form</h3>
                <form>
                    <label>
                        UserName:
                       <input type="text" name="username" value={this.state.username} onChange={this.handleChange} />
                    </label>
                    <label>
                        Pick your favorite flavor:
                        <select  name="fav" value={this.state.fav} onChange={this.handleChange}>
                            <option value="grapefruit">Grapefruit</option>
                            <option value="lime">Lime</option>
                            <option value="coconut">Coconut</option>
                            <option value="mango">Mango</option>
                        </select>
                    </label>
                    <input type="submit" value="submit" onClick={this.handleSubmit} />
                </form>
            </div>
        );
    }
}
export default Registered;