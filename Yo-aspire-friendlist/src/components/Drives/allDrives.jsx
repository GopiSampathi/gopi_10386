// npm dependencies
import React, { Component } from 'react'
import { Link } from "react-router-dom";

// component dependencies
import CreateArticle from './createDrive';

import SearchBar from '../SearchBar/searchBar';
import LeftMenu from '../Sidemenu/leftMenu';
import Navbar from '../Navbar/navbar';
import ArticleCard from '../Cards/articleCards';


import { connect } from 'react-redux'

//Action imports
import * as articleActions from '../../store/actions/driveActionCreator';
import { bindActionCreators } from 'redux';


//import service file
//importing modal
import DeleteArticleModal from '../Modals/deleteArticle.modal';


// styles/assets css file 
import '../../assets/css/BaseStyles/baseStyles.css';
import '../../assets/css/LayoutStyles/helperStyles.css';
import '../../assets/css/LayoutStyles/bodyStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';
import '../../assets/css/ModuleStyles/ArticleStyles/leftMenuStyles.css';
import '../../assets/css/BaseStyles/navStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/articleCardStyles.css';

// // pagination dependency
// import Pagination from "react-js-pagination";
// import "bootstrap-less";

//creating the create article button
function CreateArticleBtn() {
  return (
    <React.Fragment>
      <Link to="/admin/createarticle"><button className='create-article-btn'>Create article</button></Link>
    </React.Fragment>
  )
}

/**
 * All Article is to display the total number of articles 
 * And These articles are being managed by the Admin
 */
class AllArticles extends Component {
  constructor(props) {
    super(props);

  }
  componentWillMount() {
    debugger;
    this.props.articleActions.fetchDrives();
  }
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="article-container">
          <div className='allarticle-topbar flex-row'>

            <CreateArticleBtn />
            <SearchBar />
          </div>
          <div className='allarticle-mainbody-holder flex-row'>
            <div className='allarticle-mainbody-rightsection'>
              <div className='list-holder'>
                {/* {
                  (this.props.myDriveDetails) ?
                    <div>
                      <div>
                        <ArticleCard />
                      </div>
                    </div> :
                    <div>Loading</div>
                } */}
                <div>
                        <ArticleCard />
                      </div>
              </div>
            </div>
          </div>

        </div>
        <div>

        </div>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    myDriveDetails: state.articlesReducer.myDrives
  }
}

function mapDispatchToProps(dispatch) {
  return {
    articleActions: bindActionCreators(articleActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllArticles);