import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as allActions from '../../store/actions/driveActionCreator';
import '../../assets/css/ModuleStyles/ArticleStyles/readarticle.css';
import { Link } from 'react-router-dom';

class ReadArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      article: {}
    }
  }
  componentWillMount() {
    this.props.allActions.fetchArticlesById(this.props.match.params.id)
  }
  componentWillReceiveProps(newProps) {
    if (newProps.article) {
      this.setState({
        article: newProps.articleItemFromReducer
      })
    }
  }
  render() {

    return (
      <div>
        {
          (this.props.articleItemFromReducer)
            ? <div className="container"><br />
              <div>
                <h3 id="main">Drive details</h3>
                <p id="go_back"><Link to="/article"> All Articles</Link></p>
              </div><br /><hr />
              <h5>{this.props.articleItemFromReducer.heading}</h5>
              <p>Description:{this.props.articleItemFromReducer.desc}</p>
              <p>Venue:{this.props.articleItemFromReducer.where}</p><br />
              <h6 id="article_brief">Date and time : {this.props.articleItemFromReducer.when}</h6>
              <img className="drive-image" src="https://www.gcreddy.com/wp-content/uploads/2018/12/Software-Testing-Jobs-7th-December.jpg"/>
              <br></br>
              <p>Eligibility:{this.props.articleItemFromReducer.eligibility}</p>
              <br></br>
              <p>More Information : {this.props.articleItemFromReducer.more_information} </p>
            </div>
            : <div>Loading...</div>
        }
      </div>
    );
  }
}
function mapStateToProps(state) {
  console.log("i am here");
  console.log(state.articlesReducer.artcilefromAction)
  return {
    articleItemFromReducer: state.articlesReducer.artcilefromAction
  }
}
function mapDispatchToProps(dispatch) {
  return {
    allActions: bindActionCreators(allActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReadArticle);