// npm dependencies
import React, { Component } from 'react'
import { Link } from "react-router-dom";
import LeftMenu from '../Sidemenu/leftMenu';
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';


//Action imports
import * as articleActions from '../../store/actions/driveActionCreator'
import { bindActionCreators } from 'redux';

/**
 * create Article Component is used by Admin to create new articles
 */
 class ListHolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heading: '',
      brief: '',
      body: '',
      headingError:'',
      briefError:'',
      bodyError:'',
      articleResult : {}
    }
  
    this.clickMe = this.clickMe.bind(this);
  }
  handleHeadingChange = event => {
    this.setState({ heading: event.target.value }, () => {
      this.validateHeading();
    });
  };
  handleBriefChange = event => {
    this.setState({ brief: event.target.value }, () => {
      this.validateBrief();
    });
  };
  handleBodyChange = event => {
    this.setState({ body: event.target.value }, () => {
      this.validateBody();
    });
  };
  validateHeading= () => {
    const { heading } = this.state;
    this.setState({
      headingError:
      
         heading.length > 3 && heading.length< 200?  null: 'Heading length should be in the range of 3 to 200' 
         
    });
  }
  validateBrief= () => {
    const { brief } = this.state;
    this.setState({
      briefError:
        brief.length > 30 && brief.length < 500 ? null : 'Brief length should be in the range of 30 to 500'  
    });
  }
  validateBody= () => {
    const { body } = this.state;
    this.setState({
      bodyError:
       body.length > 60 && body.length <8000? null : 'Body length should be in the range of 60 to 8000'
    });
  }
  // handleInput(event) {
  //   const inputValue = event.target.value;
  //   const name = event.target.name;
  //   this.setState({
  //     [name]: inputValue
  //   })
  // }
  handleSubmit = event => {
    event.preventDefault();
    const { heading } = this.state;
    const { brief } = this.state;  
    const { body } = this.state;
    alert(`Your state values: \n 
            name: ${heading} \n 
            name: ${brief} \n 
            name: ${body} \n 
            `);
  };
  clickMe = e =>{
    this.props.createArticles.createArticle({
      heading : this.state.heading,
      brief : this.state.brief,
      body : this.state.body
    })
  }

  render() {
    if(this.props.articleResult){
      return <a href="/dashboard"  />
    }
    
    return (
      <div className="container">
        <div className="row">

          <div className="col-sm-9 col-xs-12">
            <div className="row">
              <div className="col-sm-2">
              </div>
              <div className="col-sm-8 create-article">
                <h1 className="">Add Article</h1>
                <form onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label htmlFor="heading">Heading</label>
                    <input className={`form-control ${this.state.headingError ? 'is-invalid' : ''}`}
                      type="text"
                      id="heading"
                      aria-describedby="headinghelp"
                      value={this.state.heading}
                      name="heading"
                      onChange= {this.handleHeadingChange }
                      onBlur={this.validateHeading}
                      placeholder="Enter Title" />
                    <small id="headingHelp" className="form-text text-muted" />
                    <div className='invalid-feedback'>{this.state.headingError}</div>
                  </div>
                  
                  <div className="form-group">
                    <label htmlFor="discription">Brief</label>
                    <textarea className={`form-control ${this.state.briefError ? 'is-invalid' : ''}`} 
                    id="brief"
                     name="brief" 
                     value={this.state.brief} 
                     onChange= {this.handleBriefChange }
                     onBlur={this.validateBrief} rows={3} />
                     <div className='invalid-feedback'>{this.state.briefError}</div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="discription">Description</label>
                    <textarea className={`form-control ${this.state.bodyError ? 'is-invalid' : ''}`}
                    id="brief" 
                    name="body" 
                    value={this.state.body} 
                    onChange= {this.handleBodyChange }
                    onBlur={this.validateBody} rows={6} />
                    <div className='invalid-feedback'>{this.state.bodyError}</div>
                  </div>
                </form>
                <Link to='/dashboard'> <button type="submit" className="btn btn-primary" onClick={this.clickMe} >Submit</button></Link>
              </div>
              <div className="col-sm-2">
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
function mapStateToProps(state){
  debugger;
  return{
    articleResult : state.articlesReducer.articleStatus
  }
}
function mapDispatchToProps(dispatch){
  return{
    createArticles : bindActionCreators(articleActions,dispatch)
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ListHolder);

