// npm dependencies
import React, { Component }         from "react";

//component Dependencies
import AllBulletins                 from '../Bulletins/allBulletens';
import SearchBar                    from '../SearchBar/searchBar';
import LeftMenu                     from '../Sidemenu/leftMenu';
import ListHolder                   from '../ListHolder/listHolder';
import BulletinCard                   from '../Cards/bulletinCards';
import Navbar   from '../Navbar/navbar';


import BulletinService from './bulletinservice';

//Style imports
import '../../assets/css/ModuleStyles/BulletinStyles/bulletinStyles.css';
import '../../assets/css/BaseStyles/navStyles.css';

/**
 * Bulletin Component is the Component
 * which is visible to the admin After he gets selected Manage Bulletins in leftMenu in
 */

export default class Bulletins extends Component {

  render() {
    const bulletinList = BulletinService.map(detail => 
      <BulletinCard bulletinDetail={ detail } />
    )
    return (
      <React.Fragment>
        <Navbar/>
        <div className="article-container">
          <SearchBar />
          <div className='allarticle-mainbody-holder flex-row'>
            {/* <div className='allarticle-mainbody-leftsection'>
             <LeftMenu /> 
            </div> */}
            <div className='allarticle-mainbody-rightsection'>
              <div className='list-holder'>
                { bulletinList }
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}


