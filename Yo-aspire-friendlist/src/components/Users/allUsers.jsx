    // npm dependencies
import React, { Component } from 'react';

// styles/assets css file 
import '../../assets/css/allarticles.css'
import userservice from './userservice';


/**
 * All users is used to display the total number of users 
 */

export default class AllUsers extends Component {

    constructor(props) {
        super(props);

        this.state = {
            users: userservice
        }
    }

    handleDeleteUsers(i) {
        var confirmation = window.confirm('Are you sure you wish to delete this item?');
        if (confirmation) {
            if (confirmation == true) {
                let users = [...this.state.users]
                users.splice(i, 1)
                this.setState({
                    users: users
                })
            }
            else if (confirmation == false) {
                document.write("User wants to false continue!");
            }
        }
        console.log(i);
    }

    render() {
        let user_List = this.state.users.map(user => {
            return (
                <div>
                    <div className="row articlerow">
                        <div className="col-sm-2 articlerowimg ">
                        </div>
                        <div className="col-sm-2 articlerowimg ">
                            <img src={user.img} className=" imagecorner" />
                        </div>
                        <div className="col-sm-4 userrowtext">
                            <h2 className="writer-name" id="size">{user.name} </h2>
                        </div>
                        <div className="col-sm-2 userrowtext">
                            <button type="button" className="btn btn-primary Delete"
                                onClick={index => this.handleDeleteUsers(index)}>Delete
                            </button>
                        </div>
                    </div>
                </div>
            );
        });
        return (
            <div className="col-sm-8 Articlearea">
                {user_List}
            </div>
        )
    }
}