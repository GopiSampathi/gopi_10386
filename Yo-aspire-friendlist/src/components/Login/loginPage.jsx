import React from 'react';

import Header from '../Header/Header';
import Login from './loginForm';

function LoginPage(){
	return(
		<React.Fragment>
			<Header />
			<Login />
		</React.Fragment>
	)
}

export default LoginPage;