// npm dependencies
import React, { Component }         from "react";

//component Dependencies
import SearchBar from '../SearchBar/searchBar';
import LeftMenu from '../Sidemenu/leftMenu';
import ListHolder from '../ListHolder/listHolder';
import ComplainCard from '../Cards/complainCards';
import '../../assets/css/BaseStyles/navStyles.css';
import ComplainService from './complaintservice';
import Navbar   from '../Navbar/navbar';
//style Dependencies
/**
 * Dashboard Component is the main Component
 * which is visible to the admin After he gets logged in
 */

export default class Dashboard extends Component {

  render() {
    const complaincard = ComplainService.map(detail => 
      <ComplainCard complainDetail={detail} />
    )
    return (
      <React.Fragment>
        <Navbar/>
        <div className="article-container">
          <SearchBar />
          <br/>
          <div className='allarticle-mainbody-holder flex-row'>
            {/* <div className='allarticle-mainbody-leftsection'>
             <LeftMenu /> 
            </div> */}
            <div className='allarticle-mainbody-rightsection'>
              <div className='list-holder'>
                {complaincard}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>  
    );
  }
}


