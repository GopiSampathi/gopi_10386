import React, { Component } from 'react';

import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';

class SearchBar extends Component{
	constructor(){
		super();
		this.state = {
			query:""
		}
	}

	handleInputChange = (e) =>{
		this.setState({
			query:e.target.value
		})
	}

	componentDidUpdate(){
		console.log(this.state.query);		
	}

	render(){
		return(
			<React.Fragment>
				<div className="searchbar-holder">
					<div className='searchbar-console flex-row'>
						<span><input className="search-input-txt" type='text' onChange={this.handleInputChange}></input></span>
						<button className='search-btn site-btn'>Search</button>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

export default SearchBar;
