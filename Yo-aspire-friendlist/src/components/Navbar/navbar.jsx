import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import '../../assets/css/BaseStyles/navStyles.css'
class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }

    clickMe = e => {
      debugger;
      if (window.confirm("Do you sure want to Logout?")) {
        window.location.href = '/logout';
      }
    }
    render() { 
        return (
            
            <header className="header">
 
                    <input className="menu-btn" type="checkbox" id="menu-btn" />
                    <label className="menu-icon" for="menu-btn"><span className="navicon"></span></label>
                    <ul className="menu">
                      <li><Link to="/dashboard" className="active"><i className="fa fa-dashboard size" ></i>&nbsp;Home</Link></li>
                      <li><Link to="/article" ><i className="fa fa-newspaper-o size"></i>&nbsp;MyDrives</Link></li>
                      <li><Link to="/bulletins"><i className="fa fa-handshake-o" ></i>&nbsp;MyConnections</Link></li>
                      <li><Link to="/users"><i className="fa fa-folder-open" ></i>&nbsp;Articles</Link></li>
                      <li><Link to="/complaints"><i class="fa fa-align-justify"></i>&nbsp;Bulletins</Link></li>
                      <li><Link to="/complaints"><i className="fa fa-paper-plane" ></i>&nbsp;Messages</Link></li>
                      <li onClick ={this.clickMe}><Link to=''><i className="fa fa-sign-out size" ></i >&nbsp;Logout</Link></li>
                    </ul>
                  </header>
         );
    }
}
 
export default Navbar;