import React, { Component } from 'react';

import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';

import '../../assets/css/ModuleStyles/UserStyles/userCardStyles.css';

import * as allActions from '../../store/actions/myFriendsActionCreators';

class UserCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			friendList: []
		}
	}
	componentDidMount() {
		this.props.allAction.fetchMyFriends();
	}
	render() {
		// const { userDetail } = this.props


		console.log(this.props.friendList)
		return (
			<React.Fragment>
				{
					(this.props.friendList) ?
						<div>
							<div className="article-card-holder">
								{this.props.friendList.map((items) => (
									<div className="article-card ">
										<div className='flex-row article-card-topheader'>
											<div className='img-holder'>
												<img className='article-img' src={"https://yoaspire.com:8050/api/image" + this.props.friendList.image} />
											</div>
											{/* <div className='article-txt-data flex-row '> */}											
											<p className='writer'>{items.first_name}</p>												
											{/* </div> */}
										</div>

									</div>


								)


								)}

							</div>
						</div> :
						<div>Loading..!</div>
				}
			</React.Fragment>
		)
	}
}
function mapStateToProps(state) {


	console.log(state)
	return {
		friendList: state.myFriendsReducer.friendsList
	}
}
function mapDispatchToProps(dispatch) {

	return {
		allAction: bindActionCreators(allActions, dispatch)
	};
}
export default connect(mapStateToProps, mapDispatchToProps)(UserCard);
