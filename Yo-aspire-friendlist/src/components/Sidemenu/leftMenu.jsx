import React from 'react';
import { Link } from "react-router-dom";

function clickMe() {
	debugger;
	if (window.confirm("Do you sure want to Logout?")) {
		window.location.href = '/logout';
	}
	else {
		window.location.reload();
	}
}
function LeftMenu() {
	return (
		<React.Fragment>
			<div className="leftmenu-holder">
				<ul className='leftmenu'>
					<Link to="/dashboard" className="active"><li>Dashboard</li></Link>
					<Link to="/article" ><li>Manage Articles</li></Link>
					<Link to="/bulletins"><li>Manage Bulletins</li></Link>
					<Link to="/users"><li>Manage Users</li></Link>
					<Link to="/complaints"><li>Manage Complaints</li></Link>
					{/* <Link to='/logout'><li>Logout</li></Link> */}
					<li onClick={clickMe} >Logout</li>
				</ul>
			</div>
		</React.Fragment>
	)
}

export default LeftMenu;