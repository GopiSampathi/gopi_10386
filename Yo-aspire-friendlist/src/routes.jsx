// npm dependencies
import React, { Component }             from "react";
import { Route, BrowserRouter }         from "react-router-dom";

// all components for routing
import Header                           from "./components/Header/Header";
import EditArticle                      from "./components/Drives/editDrives";
import AllBulletins                     from "./components/Cards/userCards";
import Complaints                       from "./components/Complaints/complaints";
import Users                            from "./components/Users/users";
import CreateArticle                    from "./components/Drives/createDrive";
import LoginForm                        from './components/Login/loginForm';
import AllArticles                      from './components/Drives/allDrives'
import Logout                           from "./components/Logout/logout"
import Role                             from "./components/Roles/role"
import ReadArticle                      from './components/Drives/readDrives';

/**
 * This is the routes component which .. FILL IN THE BLANKS!
 */
class Routes extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Route path="/" component={Header} />
          <Route exact path="/dashboard" component={AllArticles} />
          <Route exact path="/article" component={AllArticles} />
          <Route path="/" exact component={LoginForm} />
          <Route exact path="/bulletins" component={AllBulletins} />
          <Route exact path="/article/:id" component={ReadArticle} />
          <Route exact path="/complaints" component={Complaints} />
          <Route exact path="/users" component={Users} />
          <Route exact path="/admin/createarticle" component={CreateArticle} />
          <Route exact path="/admin/editarticle/:id" component={EditArticle} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/role" component={Role} />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default Routes;
