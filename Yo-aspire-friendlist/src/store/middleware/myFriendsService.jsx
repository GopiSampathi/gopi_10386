import request from 'superagent'

import * as allActions from '../actions/action.constants';
import * as myFriendsActions from '../actions/myFriendsActionCreators';

const friendsService = (store) => next => action => {
    next(action)
    debugger;
    switch (action.type) {
        case allActions.FETCH_MY_FRIENDS_LIST:
            request.get('https://yoaspire.com:8050/api/network/')
                .send(action.payload)
                .set('Accept', 'application/json')
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    console.log(data);
                    debugger;
                    if (data.statuscode == 1) {
                        next(myFriendsActions.receiveFriends(data));
                    }
                    else {
alert("no data")
                    }
                })
            break;

        default:
            break;
    }
}

export default friendsService;
