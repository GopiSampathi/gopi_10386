import request from 'superagent'

import * as BulletinActions from '../actions/bulletinActionCreator'
import * as allActions from '../actions/action.constants'

const bulletinsService = (store) => next => action => {
    next(action)
    switch (action.type){
        case allActions.FETCH_BULLETINS:      
            console.log(" bulletinsService");
            request.get("http://13.250.235.137:8050/api/bulletins/all")
            .send(action.payload)
            .set('Accept','application/json')
            .set('Authorization',('Bearer '+localStorage.getItem('jwt-token')))
              .then(res => {
                  const data = JSON.parse(res.text);
                  console.log("Bulletins services");
                  next(BulletinActions.receiveBulletins(data))
              })
              .catch(err => {
                  next({ type : "FETCH_BULLETINS_DATA_ERROR",err });

              });
              
              break;
        default:
             break;
    };
};

export default bulletinsService;