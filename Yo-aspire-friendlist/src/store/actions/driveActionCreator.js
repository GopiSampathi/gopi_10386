import * as allActions from './action.constants'

export function receiveDrives(data) {
    return {
        type: allActions.RECEIVE_DRIVES,
        payload: data
    };
}

export function fetchDrives() {
    return {
        type: allActions.FETCH_DRIVES,
        payload: {}
    };
}

export function receiveArticlesById(data){
    console.log(data);
    return { 
        type : allActions.RECEIVE_ARTICLES_BY_ID, 
        payload : data 
    };
}

export function fetchArticlesById(id){
   return {
        type : allActions.FETCH_ARTICLES_BY_ID,
        payload : {id}
     };
}

export function updateArticles(id,data){
    return { 
        type : allActions.UPDATE_ARTICLES,
        payload : {id,data} 
    }
}

export function receiveUpdateArticles(data){
    return { 
        type : allActions.RECEIVE_UPDATE_ARTICLES,
        payload : {data} 
    }
}
export function createArticle(data) {
    return {
        type: allActions.CREATE_ARTICLE,
        payload: data
    }
}
export function createArticleStatus(data) {
    return {
        type: allActions.CREATE_ARTICLE_STATUS,
        payload: data
    }
}

export function deleteArticle(id){
    return{ 
        type:allActions.DELETE_ARTICLE,
        payload:{id}
    }
}

