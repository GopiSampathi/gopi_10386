import * as allActions from './actionConstants'

export function doLoginUser(data) {
    return {
        type: allActions.DO_LOGIN_USER,
        payload: data
    }
}

export function loginUserSucess(data) {
    return {
        type: allActions.LOGIN_USER_SUCCESS,
        payload: data
    }
}

export function logoutUser() {
    return {
        type: allActions.LOGOUT_USER,
        payload: {}
    };
}