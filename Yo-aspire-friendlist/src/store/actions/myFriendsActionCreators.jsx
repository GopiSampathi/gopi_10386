import * as allActions from './action.constants';

export function fetchMyFriends() {
    return {
        type: allActions.FETCH_MY_FRIENDS_LIST,
        payload: {}
    }
}
export function receiveFriends(data) {
    return {
        type: allActions.RECEIVE_MY_FRIENDS_LIST,
        payload: data
    }
}