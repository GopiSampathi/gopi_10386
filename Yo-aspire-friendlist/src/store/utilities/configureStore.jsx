import { createStore,applyMiddleware,compose } from  'redux'
import rootReducer from '../reducer/rootReducer';

import authMiddleware from '../middleware/authService'

import articlesMiddleware from '../middleware/allDrivesService';

import bulletinsMiddleware from '../middleware/allBulletinService';

import roleMiddleware from '../middleware/roleService';

import myFriendsMiddleWare from '../middleware/myFriendsService';

export default function configureStore(){
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            authMiddleware,articlesMiddleware,bulletinsMiddleware,roleMiddleware,myFriendsMiddleWare
        ))
    );
};