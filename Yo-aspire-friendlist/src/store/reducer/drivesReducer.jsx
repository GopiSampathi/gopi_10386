import * as allActions from '../actions/action.constants'

const initialState = {
  myDrives:[],
  isLoaded: false,
  newsItem: {},
  articleStatus: {},
  artcilefromAction: {},
  isUpdateData: false,
  isEditLoaded: false,
  isDeleted: false,
  deleteStatusCode: 0

}

export default function articlesReducer(state = initialState, action) {
  debugger
  switch (action.type) {
    case allActions.FETCH_DRIVES:
      return action;

    case allActions.RECEIVE_DRIVES:
      return {
        ...state,
        myDrives: action.payload.drives,
        isLoaded: true
      };

    case allActions.DELETE_ARTICLE:
      return action;

    case allActions.DELETE_ARTICLE_STATUS:
      return {
        ...state,
        isDeleted: true,
        deleteStatusCodeL: action.payload
      };

    case allActions.FETCH_ARTICLES_BY_ID:
      return action;

    case allActions.RECEIVE_ARTICLES_BY_ID:
      return {
        ...state,
        artcilefromAction: action.payload.drive,
        isEditLoaded: true
      };


    case allActions.UPDATE_ARTICLES:
      debugger;

      return {
        ...state,
        [action.payload.id]: action.payload
      };

    case allActions.RECEIVE_UPDATE_ARTICLES:
      debugger;
      return {
        ...state,
        //payload data will visible in postman
        //with the same name we are calling
        articles: action.payload.article,
        isUpdateData: true
      };

    case allActions.CREATE_ARTICLE_STATUS:
      return {
        ...state,
        articleStatus: action.payload
      }
    default:
      return state;
  }
}