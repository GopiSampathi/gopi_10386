import * as allActions from '../actions/action.constants';

const initialState = {
  user: [],
  isLoaded: false,
  deleteStatusCode: 0,
  friendsList:[], 
}
export default function userReducer(state = initialState, action) {
  switch (action.type) {

      case allActions.FETCH_FRIENDS_LIST_USERS:
      return action;

    case allActions.RECEIVE_FRIENDS_LIST_USERS:
      return {
        ...state,  
        friendsList : action.payload.user.friend_list, 
      };
  
      default:
      return state; 
  }
}
