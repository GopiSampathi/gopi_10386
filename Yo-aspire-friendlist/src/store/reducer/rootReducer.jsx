import { combineReducers } from 'redux'

import articlesReducer from './drivesReducer';
import loginReducer from './authReducer';
import bulletinsReducer from './bulletinReducer';
import roleReducer from './roleReducer';
import myFriendsReducer from './myFriendReducer';

const rootReducer = combineReducers({
    loginReducer,
    articlesReducer,
    bulletinsReducer,
    roleReducer,
    myFriendsReducer
});

export default rootReducer;