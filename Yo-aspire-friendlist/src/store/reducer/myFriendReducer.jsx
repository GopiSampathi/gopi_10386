import * as allActions from '../actions/action.constants';
const initialState = {
    myFriendReducer:[],
    isLoaded:false,
    myFriendStatusCode:0,
    friendsList:[]
}
export default function myFriendsReducer(state = initialState, action){
    switch(action.type){
        case allActions.FETCH_MY_FRIENDS_LIST:
        return action;

        case allActions.RECEIVE_MY_FRIENDS_LIST:
        return{
            ...state,
            friendsList : action.payload.friends
        };
        default:
        return state;
    }
}