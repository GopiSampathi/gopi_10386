import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import * as authActions from '../../store/actions/roleAction';

class UserRole extends Component {
    constructor(props) {
        super(props);

    }

    componentWillMount() {
        var token = localStorage.getItem("jwt-token");
        debugger;
        if (token) {
            this.props.authActions.doFetchUser(token);
        }

    }
    render() {
        debugger;
        if (this.props.isUserRole) {
            if (this.props.userRole.role == "user") {
                if (window.confirm("you are not an recruiter")) {
                    window.location.href = '/';
                }
            }
            else if (this.props.userRole.role == "admin") {
                if (window.confirm("you are not an recruiter")) {
                    window.location.href = '/';
                }
            }
            else if (this.props.userRole.role == "recruiter") {
              return <Redirect to='/dashboard' />
            }
        }
        return (
            <h1>Loading.....</h1>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        userRole: state.roleReducer.userRoleData,
        isUserRole: state.roleReducer.isUserRole
    };
}

function mapDispatchToProps(dispatch) {

    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(UserRole);
