// npm dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

//Action imports
import * as articleActions from '../../store/actions/articleActionCreator'
import { bindActionCreators } from 'redux';

//component dependencies
import Navbar from '../Navbar/navbar';

// styles/assets css file 
import '../../assets/css/BaseStyles/navStyles.css';

/**
 * Edit Article Component is used by the Admin to edit the 
 * existing articles
 */
class EditArticle extends Component {

  constructor() {
    super();
    this.state = {
      articleData: [],
      title: "",
      by: {

        first_name: "",
        last_name: ""
      },
      discription: ""
    };
  }

  componentWillMount() {
    this.props.articleActions.fetchArticlesById(this.props.match.params.id);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.article) {
      this.setState({
        //1st 
        articleData: newProps.article,
        title: newProps.article.heading,
        by: {
          first_name: newProps.article.by.first_name,
          last_name: newProps.article.by.last_name,
        },
        discription: newProps.article.brief

      })
    }
  }

  handleInut(event) {

    const inputValue = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: inputValue
    })

  }
  updateArticle() {
    debugger;
    var updatedData = {

      tags: [0],
      brief: this.state.discription,
      body: 'Just like XML, JSX tags have a tag name, attributes, and children. If an attribute value is enclosed in quotes, the value is a string. Otherwise, wrap the value in braces and the value is the enclosed JavaScript expression'
    }
    console.log(updatedData);
    this.props.articleActions.updateArticles(this.props.match.params.id, updatedData);
    this.setState({
      isSubmitted: true
    })

  }
  render() {
    console.log("Edit article", this.state.articleData)
    return (
      <div>
        <Navbar />
        <React.Fragment>
          {
            (this.props.isEditLoaded) ?
              <div>
                <div className="row">
                  <div className="col-sm-8 col-xs-12">
                    <div className="row">
                      <div className="col-sm-2">
                      </div>
                      <div className="col-sm-8 create-article">
                        <h1 className="">Edit Article</h1>
                        <form>
                          <div className="form-group">
                            <label for="title">Title</label>
                            <input type="text" className="form-control" defaultValue={this.state.articleData.heading} id="title" aria-describedby="titlehelp" name="title" onChange={(event) => this.handleInut(event)} placeholder="Enter Title" />
                            <small id="titleHelp" className="form-text text-muted"></small>
                          </div>
                          <div className="form-group">
                            <label for="publisher">Publisher First Name</label>
                            <input type="text" className="form-control" defaultValue={this.state.articleData.by.first_name} id="publisher" name="first_name" onChange={(event) => this.handleInut(event)} placeholder="Publisher" />
                          </div>
                          <div className="form-group">
                            <label for="publisher">Publisher Last Name</label>
                            <input type="text" className="form-control" defaultValue={this.state.articleData.by.last_name} id="publisher" name="last_name" onChange={(event) => this.handleInut(event)} placeholder="Publisher" />
                          </div>
                          <div className="form-group">
                            <label for="discription">Discripton</label>
                            <textarea className="form-control" id="discription" name="discription" onChange={(event) => this.handleInut(event)} defaultValue={this.state.articleData.brief} rows="3"></textarea>
                          </div>
                        </form>
                        <button type="submit" className="btn btn-primary" onClick={this.updateArticle.bind(this)}>Update</button>
                      </div>
                      <div className="col-sm-2">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              :
              <div>Loading</div>
          }
        </React.Fragment>
      </div>

    )
  }
}

function mapStateToProps(state) {
  return {

    article: state.articlesReducer.artcilefromAction,
    isEditLoaded: state.articlesReducer.isEditLoaded,
    isUpdated: state.articlesReducer.isUpdateData
  }
}

function mapDispatchToProps(dispatch) {
  return {
    articleActions: bindActionCreators(articleActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditArticle)