// npm dependencies
import React, { Component } from 'react'
import { Link } from "react-router-dom";

// component dependencies
import CreateArticle from './createArticle';

import SearchBar from '../SearchBar/searchBar';
import LeftMenu from '../Sidemenu/leftMenu';
import Navbar   from '../Navbar/navbar';
import ListHolder from '../ListHolder/listHolder';

import '../../assets/css/BaseStyles/navStyles.css';

//importing modal
import DeleteArticleModal from '../Modals/deleteArticle.modal';

// styles/assets css file 

/**
 * All Article is to display the total number of articles 
 * And These articles are being managed by the Admin
 */
export default class AllArticles extends Component{
  constructor(){
    super();}

    render(){
      return(
        <React.Fragment>
          <Navbar/>
          <div className="article-container">
         
            <div className='allarticle-mainbody-holder flex-row'>
            
              {/* <div className='allarticle-mainbody-leftsection'>
               <LeftMenu /> 
              </div> */}
              <div className='allarticle-mainbody-rightsection'>
                <ListHolder />
              </div>
            </div>
          </div>
        </React.Fragment>
      )
    }
}