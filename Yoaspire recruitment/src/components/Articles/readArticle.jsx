import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as allActions from '../../store/actions/articleActionCreator';
import '../../assets/css/ModuleStyles/ArticleStyles/readarticle.css';
import { Link } from 'react-router-dom';

class ReadArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      article: {}
    }
  }
  componentWillMount() {
    this.props.allActions.fetchArticlesById(this.props.match.params.id)
  }
  componentWillReceiveProps(newProps) {
    if (newProps.article) {
      this.setState({
        article: newProps.articleItemFromReducer
      })
    }
  }
  render() {

    return (
      <div>
        {
          (this.props.articleItemFromReducer)
            ? <div className="container"><br />
              <div>
                <h3 id="main">Article details</h3>
                <p id="go_back"><Link to="/article"> All Articles</Link></p>
              </div><br /><hr />
              <h5>{this.props.articleItemFromReducer.heading}</h5>
              <p>By:{this.props.articleItemFromReducer.by.first_name}{this.props.articleItemFromReducer.by.last_name}</p>
              <p>Posted on:{this.props.articleItemFromReducer.timestamp}</p><br />
              <h6 id="article_brief"> {this.props.articleItemFromReducer.brief}</h6>
              {/* space for image  */}<br/><br/><br/><br/><br/>
              <p>{this.props.articleItemFromReducer.body}</p>
            </div>
            : <div>Loading...</div>
        }
      </div>
    );
  }
}
function mapStateToProps(state) {
  console.log("i am here");
  console.log(state.articlesReducer.artcilefromAction)
  return {
    articleItemFromReducer: state.articlesReducer.artcilefromAction
  }
}
function mapDispatchToProps(dispatch) {
  return {
    allActions: bindActionCreators(allActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReadArticle);