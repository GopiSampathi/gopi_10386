import React, { Component } from 'react';

// Style import
import '../../assets/css/ModuleStyles/ModalStyles/modalStyles.css';

class DeleteArticleModal extends Component{
	constructor(){
		super();
		this.state = {
			modalDisplay:true
		};
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal(){
		this.setState({
			modalDisplay:false
		})
	}
	render(){
		let deleteModalState; 
		(this.state.modalDisplay) ? deleteModalState = "modal-content ":deleteModalState="nodisplay"
		return(
			<React.Fragment>
				<div className={deleteModalState}>
					<div className="modal-body flex-row">
					    <p>Do you want to delete the article?</p>
					    <div className="cancel-deletearticle-btn-holder">
					    	<button type="button" className="btn btn-primary DeleteConfirm hasborder">Delete</button>
					    	<button type="button" className="btn btn-primary CancelOption hasborder"  onClick={ this.closeModal }>Cancel</button>
					    </div>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

export default DeleteArticleModal;