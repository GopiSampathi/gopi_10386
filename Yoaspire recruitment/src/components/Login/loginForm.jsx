//npm Dependencies
import React, { Component } from 'react'
import { Link } from 'react-router-dom';

//stylesheet for login form
import '../../assets/css/BaseStyles/baseStyles.css';
import '../../assets/css/ModuleStyles/SignInStyles/login.css';

//Actions for authentication
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom';

import * as authActions from '../../store/actions/authActionCreator'

/**
 * Login Form for the Admin
 */
import { bindActionCreators } from 'redux';
class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fields: {
      },
      email: "",
      password: "",
      errors: {},
      isSubmitted: false

    }
    this.handleChange = this.handleChange.bind(this);
    this.doLogin = this.doLogin.bind(this);
  }

  handleChange(event) {

    let fieldsValue = event.target.value;;
    let name = event.target.name;
    this.setState({

      [name]: fieldsValue

    });


    event.preventDefault();
  }

  doLogin = e => {
    this.props.authActions.doLoginUser({
      email: this.state.email,
      password: this.state.password
    })
    this.setState({
      isSubmitted: true
    })
  }

  render() {

    if (this.state.isSubmitted) {
      if (localStorage.getItem('jwt-token') && this.props.isLoggedIn == true) {
        
        return <Redirect to='/role' />
      }
      else {
        if (this.props.statuscode == -1) {
          alert("Your email and password incorrect");
          this.setState({
            isSubmitted: false,
            email: "",
            password: "",
          })
        }
        else if (this.props.statuscode == -2) {
          alert("your password is incorrect");
          this.setState({
            isSubmitted: false,
            password: '',
          })
        }
        else {

        }
      }
    }
    return (
      <div className="username-password-formholder">
        <div className="username-password-form flex-column">
          <form>
            <p className="subheading-txt">Username:</p>
            <input className='username-txt-field' name="email" refs="email" value={this.state.email} type="email" onChange={this.handleChange.bind(this)}  ></input>
            <p className="subheading-txt">Password: </p>
            <input className='password-txt-field' name="password" refs="password" value={this.state.password} type='password' onChange={this.handleChange.bind(this)} ></input>
            <span className={(this.props.statuscode == -2 ? 'show' : 'hidden')}>Password is incorrect </span>
          </form>
          <input type="submit" onClick={this.doLogin} name="submit" value="Submit" />
        </div>
      </div>
    );
  }


}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.loginReducer.is_logged_in,
    statuscode: state.loginReducer.statuscode
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);