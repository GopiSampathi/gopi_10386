import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import * as allActions from '../../store/actions/articleActionCreator';

import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';

import DeleteArticle from '../Modals/deleteArticle.modal';

class ArticleCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isDeleted: {},
            myDriveDetails: []
        }
    }

    handleClick(id, e) {
        if (window.confirm("Do you want to delete article")) {
            this.props.allActions.deleteArticle(id);
            setTimeout(() => {
                window.location.reload()
            }, 100)
        }
    }
    render() {
        let drivesCompleted;
        if (this.props.myDriveDetails) {
             drivesCompleted = this.props.myDriveDetails.filter(driveDetails => driveDetails.is_conducted == true);
        }

        return (
            <React.Fragment>
                {
                    (this.props.myDriveDetails) ?
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="article-card-holder changes">

                                        {this.props.myDriveDetails.map((driveDetails) => (
                                            <div className="article-card ">
                                                <div className='flex-row article-card-topheader'>
                                                    <div className='img-holder'>
                                                        <img className='article-img' src={driveDetails.image} />
                                                    </div>
                                                    <div className='flex-column article-txt-data'>
                                                        <p className='article-header-txt'>{driveDetails.heading}</p>
                                                        <p className='writer'>Venue : {driveDetails.where}</p>
                                                        <p className='article-brief'>Apply : {driveDetails.url}</p>
                                                        <p className='article-header-txt'>Last Day:{driveDetails.register_before}</p>
                                                        <div className='btn-holder'>
                                                            <Link to={`article/${driveDetails._id}`}>   <button className="edit-btn site-btn">Read</button></Link>
                                                            <Link to={`/admin/editarticle/${driveDetails._id}`} > <button className="edit-btn site-btn">Edit</button> </Link>
                                                            <button onClick={this.handleClick.bind(this, driveDetails._id)}>Delete</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}

                                    </div>
                                </div>
                                <div className="col-md-6">
                                    {
                                        (drivesCompleted == null) ?

                                            <div className="article-card-holder changes">
                                                {drivesCompleted.map(driveDetails => (
                                                    <div className="article-card ">
                                                        <div className='flex-row article-card-topheader'>
                                                            <div className='img-holder'>
                                                                <img className='article-img' src={driveDetails.image} />
                                                            </div>
                                                            <div className='flex-column article-txt-data'>
                                                                <p className='article-header-txt'>{driveDetails.heading}</p>
                                                                <p className='writer'>Venue : {driveDetails.where}</p>
                                                                <p className='article-brief'>Apply : {driveDetails.url}</p>
                                                                <p className='article-header-txt'>Last Day:{driveDetails.register_before}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div> :
                                            <div>completed drives null..</div>
                                   }
                                </div>
                            </div>
                        </div> :
                        <div>Loading...!</div>
                }

            </React.Fragment>
        )
    }
}
function mapStateToProps(state) {
    debugger;
    return {
        statuscode: state.articlesReducer.deleteStatusCode,
        isDeleted: state.articlesReducer.isDeleted,
        myDriveDetails: state.articlesReducer.myDrives
    }
}
function mapDispatchToProps(dispatch) {

    return {
        allActions: bindActionCreators(allActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleCard);







