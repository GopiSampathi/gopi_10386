import React, { Component } from 'react';

export default class BulletinCard extends Component{

constructor(props){
       super(props);
}

render(){
const { bulletinDetail } = this.props;
//let date = (new Date(articleDetail.time)).toString().slice(4,15)
return(
<React.Fragment>
<div className="article-card-holder">	
<div className="article-card ">
<div className='flex-row article-card-topheader'>
<div className='img-holder'>
<img className='article-img' src={bulletinDetail.by.image}/>
</div>
<div className='flex-column article-txt-data'>
<p className='article-header-txt'>{bulletinDetail.heading}</p>
<p className='writer'>{bulletinDetail.by.first_name}  {bulletinDetail.by.last_name}</p>

<p className='article-brief'>{bulletinDetail.brief}</p>
<div className='btn-holder'>
{/* <button className="edit-btn site-btn">Edit</button>	 */}
<button className="delete-btn site-btn" onClick ={this.props.DeleteArticle}>Delete</button>	
</div>
</div>
</div>
</div> 
</div>
</React.Fragment>
   	)	
}
}




