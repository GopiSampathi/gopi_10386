// npm dependencies
import React, { Component } from "react";
import { Link } from "react-router-dom";

//component Dependencies
import AllArticles from '../Articles/allAarticle';
import LeftMenu from '../Sidemenu/leftMenu';

//style Dependencies


/** 
 * Dashboard Component is the main Component
 * which is visible to the admin After he gets logged in
 */

class Dashboard extends Component {

  render() {  
    return (
     <React.Fragment>
        <div className='article-container'>
          <div className='allarticle-mainbody-holder flex-row'>
            <div className='allarticle-mainbody-leftsection'>
              <LeftMenu />
            </div>
          </div>
        </div>
     </React.Fragment>
    )
  }
}

export default Dashboard;
