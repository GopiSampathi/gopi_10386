// Action constants for auth scenario
export const DO_LOGIN_ADMIN = 'DO_LOGIN_ADMIN';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';

// Actions constants for bulletin scenario  
export const DELETE_BULLETIN = 'DELETE_BULLETIN';

// Actions constants for user scenario  
export const DELETE_USER = 'DELETE_USER';

// Actions constants for complain scenario  
export const RESOLVE_USER = 'RESOLVE_USER';

// Action constants for article scenario
export const CREATE_ARTICLE = 'CREATE_ARTICLE';
export const EDIT_ARTICLE = 'EDIT_ARTICLE';

/*
 *Action constatns for Login service 
 */
export const DO_LOGIN_USER = 'DO_LOGIN_USER'
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCESS'

export const LOGIN_USER_DATA_ERROR='LOGIN_USER_DATA_ERROR';

/**
 * Logout Service
 */
export const LOGOUT_USER = 'LOGOUT_UER'

/*
*Actions constants for viewArticleService
*/
export const FETCH_DRIVES = 'FETCH_DRIVES'
export const RECEIVE_DRIVES = 'RECEIVE_DRIVES'
export const CREATE_ARTICLE_STATUS = 'CREATE_ARTICLE_STATUS'

/*
*Actions constants for Role
*/

export const FETCH_USER='FETCH_USER';
export const RECIEVE_USER='RECIEVE_USER';

/**
 * Actions constants for Updating Articles
 */
export const UPDATE_ARTICLES = 'UPDATE_ARTICLES'
export const RECEIVE_UPDATE_ARTICLES = 'RECEIVE_UPDATE_ARTICLES'

/*
*Actions constants for viewBulletinService
*/
export const FETCH_BULLETINS = 'FETCH_BULLETINS'
export const RECEIVE_BULLETINS = 'RECEIVE_BULLETINS'

export const FETCH_ARTICLES_BY_ID = 'FETCH_ARTICLES_BY_ID '
export const RECEIVE_ARTICLES_BY_ID = 'RECEIVE_ARTICLES_BY_ID '

/*
*Actions constants for delete the article
*/
export const DELETE_ARTICLE = 'DELETE_ARTICLE'
export const DELETE_ARTICLE_STATUS=' DELETE_ARTICLE_STATUS'