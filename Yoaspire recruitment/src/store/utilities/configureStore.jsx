import { createStore,applyMiddleware,compose } from  'redux'
import rootReducer from '../reducer/rootReducer';

import authMiddleware from '../middleware/authService'

import articlesMiddleware from '../middleware/allArticlesService';

import bulletinsMiddleware from '../middleware/allBulletinService';

import roleMiddleware from '../middleware/roleService';

export default function configureStore(){
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            authMiddleware,articlesMiddleware,bulletinsMiddleware,roleMiddleware
        ))
    );
};