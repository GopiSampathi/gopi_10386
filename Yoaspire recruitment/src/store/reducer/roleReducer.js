import * as allActions from '../actions/action.constants';

const initialState ={
       tokenId:{},
       userRoleData:{},
       isUserRole:false,   
}

export default function roleReducer(state = initialState, action) {

    switch (action.type) {
          
        case allActions.FETCH_USER:
            return action;
        case allActions.RECIEVE_USER:
     
            return {
                ...state, 
                userRoleData: action.payload.user,
                isUserRole:true
            };
      
        default: return state;
    }
}