import { combineReducers } from 'redux'

import articlesReducer from './articleReducer'
import loginReducer from './authReducer';
import bulletinsReducer from './bulletinReducer';
import roleReducer from './roleReducer';

const rootReducer = combineReducers({
    loginReducer,
    articlesReducer,
    bulletinsReducer,
    roleReducer
});

export default rootReducer;