import request from 'superagent'

import * as articleActions from '../actions/articleActionCreator'
import * as allActions from '../actions/action.constants'

const articlesService = (store) => next => action => {

    console.log(action.payload.id);
    next(action)
    switch (action.type) {

        case allActions.FETCH_DRIVES:
            console.log("articlesService");
            debugger;
            request.get("https://yoaspire.com:8050/api/drives/mine")
                .send(action.payload)
                .set('Accept', 'application/json')
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    console.log(data);
                    next(articleActions.receiveDrives(data))
                })
                .catch(err => {
                    next({ type: "FETCH_ARTICLES_DATA_ERROR", err });

                });

            break;

        case allActions.DELETE_ARTICLE:
            request.delete("http://13.250.235.137:8050/api/article/" + `${action.payload.id}`)
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statuscode == 1) {
                        next({
                            type: allActions.DELETE_ARTICLE_STATUS,
                            payload: data.statuscode,
                        })
                    }
                })
                .catch(err => {
                    next({ type: "DELETE_ARTICLES_DATA_ERROR", err });

                });
            break;
        case allActions.CREATE_ARTICLE:
            request.post('https://yoaspire.com:8050/api/article')
                .send(action.payload)
                .set('Accept', 'application/json')
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statuscode == 1) {
                        next(articleActions.createArticleStatus(data))
                    }
                })
                .catch(err => {
                    next({
                        type: "posting tha New article Error",
                        err
                    })
                })

            break;

        case allActions.FETCH_ARTICLES_BY_ID:
            debugger;
            request.get("http://13.250.235.137:8050/api/article" + `/${action.payload.id}`)
                .send(action.payload)
                .set('Accept', 'application/json')
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    next(articleActions.receiveArticlesById(data))
                })
                .catch(err => {
                    next({ type: "FETCH_ARTICLES__BY_ID_DATA_ERROR", err });

                });

            break;

        case allActions.UPDATE_ARTICLES:   
        console.log(action.payload.data)     
            request.put("http://13.250.235.137:8050/api/article" + `/${action.payload.id}`)
                .send(action.payload.data)
                .set('Accept', 'application/json')
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    debugger;
                    console.log(action.payload);
                    const data = JSON.parse(res.text);
                    console.log(data);
                    next(articleActions.receiveUpdateArticles(data))
                })
                .catch(err => {
                    next({ type: "FETCH_ARTICLES__BY_ID_DATA_ERROR", err });

                });

            break;

        default:
            break;
    };
};

export default articlesService;