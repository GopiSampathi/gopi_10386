import React, { Component } from 'react';

import { BrowserRouter ,Route,Switch} from 'react-router-dom';
// import CarDetails from './components/cardetails';
// import Car from './components/car';
import News from './components/news';
import NewDetails from './components/news_details'
import ProductDisplay from './components/productlist';
import Login from './components/login';
import Dashboard from './components/dashboard';
import User from './components/user';

const  Routes =()=>(

    <BrowserRouter>
    <Switch>
    <Route path="/news" exact component={News}/>
    <Route path="/news/:id" exact component={NewDetails}/>
    <Route path="/productlist" exact component={ProductDisplay}/>
    <Route path="/" exact component={Login}/>
    <Route path="/admin/dashboard" exact component={User}/>
    </Switch>
    </BrowserRouter>
)

export default Routes;
