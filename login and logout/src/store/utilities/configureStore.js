import { createStore, applyMiddleware,compose } from 'redux';
import rootReducer from '../reducer/rootReducer';
import newsMiddleware from '../middleware/newsService';
import authMiddleware from '../middleware/authService';
import userMiddleware from '../middleware/userService';

export default function configureStore(){

    return createStore(
        rootReducer,
        compose(applyMiddleware(
            newsMiddleware,
            authMiddleware,
            userMiddleware
        ))
    );
};