import * as allActions from '../actions/actionConstants';

const initialState ={
       tokenId:{}
}

export default function authReducer(state = initialState, action) {

    switch (action.type) {
    
        case allActions.DO_LOGIN_USER :          
            return action;
        case allActions.DO_LOGIN_SUCCESS:     
            return {
                ...state, 
               is_logged_in:true
            };
        default: return state;
    }
}