import * as allActions from '../actions/actionConstants';

const initialState ={
    news :[],
    isLoaded:false,
    newsItem:{},
    productList:{},
    tokenId:{}
}

export default function newsReducer(state = initialState, action) {

    switch (action.type) {
    
        case allActions.FETCH_PRODUCT_LIST :
          
            return action;
        case allActions.RECEIVE_PRODUCT_LIST:
     
            return {
                ...state, 
                productList: action.payload,
            };
        case allActions.FETCH_NEWS_BY_ID:
            return action;
        case allActions.RECEIVE_NEWS_BY_ID:
     
            return {
                ...state, 
                newsItem: action.payload.article,
            };
        
        case allActions.FETCH_NEWS:           
            return action;
        case allActions.RECEIVE_NEWS:
     
            return {
                ...state, 
                news: action.payload.articles,
            };

        case allActions.FETCH_DO_LOGIN :          
            return action;
        case allActions.RECEIVE_DO_LOGIN:     
            return {
                ...state, 
                tokenId: action.payload,
            };
        default: return state;
    }
}