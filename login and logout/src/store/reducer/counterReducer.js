import * as allActions from '../actions/actionConstants';

const initialState = {
    count:0
};

export default function counterReducer(state = initialState,action){
    switch (action.type) {
        case allActions.INCREMENT:
            return { count: state.count + 1 };
        case allActions.DECREMENT:
            return { count: state.count - 1 };
        default: return state;
    }
}