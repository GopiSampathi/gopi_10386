import * as allActions from '../actions/actionConstants';

const initialState = {
    users: {},
}

export default function userReducer(state = initialState, action) {

    switch (action.type) {

        case allActions.FETCH_USER:
            return action;
        case allActions.RECEIVE_USER:
            return {
                ...state,
                users: action.payload.user,
            };

        default: return state;
    }
}