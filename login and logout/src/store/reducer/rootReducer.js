import {combineReducers}  from 'redux';
import counterReducer from './counterReducer';
import newsReducer from './newsReducer';
import authReducer from './authReducer';
import userReducer from './userReducer';

const rootReducer = combineReducers({
    counterReducer,
    newsReducer,
    authReducer,
    userReducer

});

export default rootReducer;