import * as allActions from './actionConstants';

export function receiveNews(data) {
    
    console.log("Receive actions");
    console.log(data);
    return { type: allActions.RECEIVE_NEWS, payload: data };
}

export function fetchNews() {
    
    console.log("fetch actions");
    return { type: allActions.FETCH_NEWS, payload: {} }
}

export function receiveNewsById(data) {
    
    return { type: allActions.RECEIVE_NEWS_BY_ID, payload: data };
}

export function fetchNewsById(id) {
    
    return { type: allActions.FETCH_NEWS_BY_ID, payload: {id} }
}


export function receiveProductList(data) {
    
    return { type: allActions.RECEIVE_PRODUCT_LIST, payload: data };
}

export function fetchProductList() {
    
    return { type: allActions.FETCH_PRODUCT_LIST, payload: {} }
}

export function fetchNewsdoLogin(data) {
    
    return { type: allActions.FETCH_DO_LOGIN, payload: {data} }
}

export function receiveDoLogin(data) {
    
    return { type: allActions.RECEIVE_DO_LOGIN, payload: data };
}