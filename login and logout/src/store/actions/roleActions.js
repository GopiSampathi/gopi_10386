import * as allActions from './actionConstants';

export function receiveUser(data) {
    console.log("Receive actions");
    console.log(data);
    return { type: allActions.RECEIVE_USER, payload: data };
}

export function fetchUser() {
    console.log("fetch actions");
    return { type: allActions.FETCH_USER, payload: {} }
}