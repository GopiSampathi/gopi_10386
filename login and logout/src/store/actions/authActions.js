import * as allActions from './actionConstants';

export function doLoginUser(data) {
    
    return { type: allActions.DO_LOGIN_USER, payload: {data} }
}

export function doLoginSuccess(data) {

        return { type: allActions.DO_LOGIN_SUCCESS, payload: data };
}