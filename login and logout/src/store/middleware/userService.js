import request from 'superagent';
import * as allActions from '../actions/actionConstants';
import * as roleActions from '../actions/roleActions';


const userService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_USER:
            request.get("http://13.250.235.137:8050/api/user")
                .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    next(roleActions.receiveUser(data));
                    console.log("token sent");
                }
                )
                
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_USER_ERROR', err });
                })

            break;


        default:
            break;
    }
}
export default userService;