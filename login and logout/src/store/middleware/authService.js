import request from 'superagent';
import * as newsActions from '../actions/authActions';
import * as allActions from '../actions/actionConstants';

const authService = (store) => next => action => {
   
    next(action) 
  
    switch (action.type) {  
          
           case allActions.DO_LOGIN_USER :
            console.log(action.payload);
           
            request.post('http://13.250.235.137:8050/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({"email":action.payload.data.email,"password":action.payload.data.password})            
                .then(res => {
                    console.log("success");
                    
                    const data = JSON.parse(res.text);
                    if(data.statuscode == 1){
                        next({
                            type:allActions.DO_LOGIN_SUCCESS,
                            payload:data.statuscode
                        })
                    localStorage.setItem('jwt-token',res.body.token)
                    console.log("token established");

                    }
                    else{
                        console.log("error defined");
                        next({
                            type:'LOGIN_USER_DATA_ERROR',
                            payload:data.statuscode
                        })
                    }
                })
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_NEWS_DATA_ERROR', err });
                });
            break;
        default: break;
    };
};

export default authService;