import request from 'superagent';
import * as newsActions from '../actions/newsActions';
import * as allActions from '../actions/actionConstants';

const newsService = (store) => next => action => {
   
    next(action) 
    switch (action.type) {
        case allActions.FETCH_NEWS:
            console.log('news service');
            request.get('http://13.229.176.226:8001/api/news/recent')
                .then(res => {
                    console.log("success");
               
                    const data = JSON.parse(res.text);
                    next(newsActions.receiveNews(data));
                    console.log(data);
                })
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_NEWS_DATA_ERROR', err });
                });
            break;

            case allActions.FETCH_NEWS_BY_ID:
           
            request.get('http://13.229.176.226:8001/api/news'+`/${action.payload.id}`)
                .then(res => {
                    const data = JSON.parse(res.text);
                    debugger;
                    next(newsActions.receiveNewsById(data));
                  
                })
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_NEWS_DATA_ERROR', err });
                });
            break;

            case allActions.FETCH_PRODUCT_LIST :
            console.log('news service');
            request.get('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
                .then(res => {
                    console.log("success");
               
                    const data = JSON.parse(res.text);
                    next(newsActions.receiveProductList(data));
                    console.log(data);
                })
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_NEWS_DATA_ERROR', err });
                });
            break;

            case allActions.FETCH_DO_LOGIN :
            console.log(action.payload);
            debugger;
            request.post('http://13.250.235.137:8050/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({"email":action.payload.data.email,"password":action.payload.data.password})            
                .then(res => {
                    console.log("success");
                    debugger;
                    const data = JSON.parse(res.text);
                    next(newsActions.receiveDoLogin(data));
                    console.log(data);
                })
                .catch(err => {
                    console.log("service failure");
                    next({ type: 'FETCH_NEWS_DATA_ERROR', err });
                });
            break;
        default: break;
    };
};

export default newsService;