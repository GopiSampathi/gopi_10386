import React, { Component } from 'react';
import './App.css';
import News from './components/news';
import Routes from './route';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes/>
      </div>
    );
  }
}

export default App;
