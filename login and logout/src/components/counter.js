import React from 'react';
import { connect } from 'react-redux';

class Counter extends React.Component{

    increment=()=>{
        this.props.dispatch({ type:'INCREMENT'});
        console.log(this.props.count);
  
    }

    decrement=()=>{
        this.props.dispatch({ type:'DECREMENT'});
  
    }

    render(){
        return(
             <div>
                 <h2>Counter</h2>
                 <div>
                    <button onClick={this.decrement}>-</button><br></br>
                    <span>{this.props.count}</span><br></br>
                    <button onClick={this.increment}>+</button>
                 </div>
             </div>
        )
    }
}

function mapStateToProps(state) {
    return {
      count: state.counterReducer.count
    };
  }
  
  export default connect(mapStateToProps)(Counter);