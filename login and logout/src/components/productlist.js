import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as newsActions from '../store/actions/newsActions'
import santoor from '../santoor.jpg';

class DisplayProducts extends Component {
    componentDidMount() {
        this.props.newsActions.fetchProductList();
        debugger;
    }
    render() {
        let listOfData
console.log(this.props.productList);
debugger
        if(this.props.productList )
        {
            listOfData=this.props.productList.map((item) => {
          
                return <div className="card col-md-3" style={{margin:"40px"}}>
                    <img class="card-img-top" src={santoor} style={{ width:"200px",padding:"20px",marginLeft:"25px"}} alt="Card image" />
                    <div className="card-body " style={{ textAlign:"center"}}>
                    <h4 class="card-title"><b>Name: </b>{item.productname}</h4>
                        <p class="card-text"><b>Price: </b>{item.price}</p>
                        <p class="card-text"><b>Quantity: </b>{item.quantity}</p>
                                            
                        
                    </div>
                </div>
           
    
          });
        }
    

        return (  
        <div style={{marginTop:"50px"}}>
            <div className="row">             
                  {listOfData}             
            </div>
        
            </div>

        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        productList: state.newsReducer.productList,
    };
}

function mapDispatchToProps(dispatch) {

    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(DisplayProducts);