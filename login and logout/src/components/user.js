import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as roleActions from '../store/actions/roleActions';
import {Redirect} from 'react-router-dom';

class User extends Component{
    constructor (props){
        super(props);
        this.state = {
            valid : false
        }
    }
    logout = e => {
        localStorage.removeItem ('jwt-token');
        this.setState ({valid : true})
    }

    componentDidMount(){
        this.props.roleActions.fetchUser();
    }
    render() { 
        if (this.state.valid === true){
            return (
                <Redirect to = '/' />
            )
        }
        return ( 
            <div>
            {
                (this.props.user)
                    ? <div>
                       { this.props.user.role}
                       <div>
                           <button onClick = {this.logout}> LOGOUT</button>
                        </div>
                    </div>
                    : <div>
                        loading...
                    </div>
            }
        </div>
         );
    }
}
function mapStateToProps(state){
    console.log ('i am here' + state.userReducer.users)
    return{
    user : state.userReducer.users,
    };
}

function mapDispatchToProps(dispatch){

    return{
        roleActions: bindActionCreators(roleActions,dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(User);