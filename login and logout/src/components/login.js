import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import './login.css';
import * as authActions from '../store/actions/authActions'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {            
            email:"",
            password:"",
            isSubmitted:false
          }

  
    }
    handleInut(event) {
      
        const inputValue = event.target.value;
        const name = event.target.name;
        this.setState({
        
            [name]: inputValue
          
        })
    
      }
    
    onSubmit(e){
        
        e.preventDefault();
        const loginCredential={
            email:this.state.email,
            password:this.state.password
        }
        this.setState({
            isSubmitted:true
        })
        this.props.authActions.doLoginUser(loginCredential);  
        console.log(loginCredential);
    }
    render() { 
        
        if(this.state.isSubmitted){
            console.log(this.props.isLoggedIn )
            if(localStorage.getItem('jwt-token') && this.props.isLoggedIn == true){
                return <Redirect to='/admin/dashboard'/>
            }
        }
        return ( 
            <div className="container pt-3">
  <div className="row justify-content-sm-center">
    <div className="col-sm-10 col-md-6 col-sm-8">
      <div className="card border-info">
        <div className="card-header">Sign in to continue</div>
        <div className="card-body">
          <div className="row">
            <div className="col-md-4 text-center">
              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwAAAB9CAMAAABH0HuwAAAAM1BMVEX///8EBAR/f38TExPu7u7Ozs6/v78iIiIxMTGfn59vb29QUFBgYGBBQUGurq7f39+Pj4/f3DUiAAADLUlEQVR4nO1b2bKrIBCURdwT//9r7zUeoyKBEQZOV530Y8ok7TDMPlX1xRd/F7Xp5BudqX+JhjZyaMQFzSCNLsvE9O2Vx462LyYiMyofkxVqNPmZaOk4Gzcamfe8niOVyYrxCUMlI50YKrnoSILWuqEkMxVDVlsXGtab1adQWdCzUam9Fo6GlskMPqK15Qj14OAiOagsYNDjyAvtwphIRTOoy442yT/wckljw80lhQ0/lwQ2Az8XIYY4Loz36IioO9Xl4SJEd59LnYuLELc9g2bxAW6ou0qcRXk33FTiOScXIeY7XJ4ZD2mBuhOKZj2kBTcOyuTmIgQ9EE0KeGloqFyymbsjqKavgGDIoikiGKpoigiGKJpCgqGJZipFZgpzyeitbYS998dMVg2yM8Z0cuDyFeGc1/1P6lSlq3ueJDPE5eH8VneJQDoOOqGM13VKvSsY0smViXA4fDUyHzN2kyycgKm53qXms86nl0r89+kS4TW+eDU5y/NHfHZUpfzc68ST8sdYtsqE9D0xDPMqjbYeDpulxDvlUwLrRQkJTmJ+5Ys+rZIZpfCV5uR9/3AWOi3zk3dgXxCfHkzkJyNhGwNfGHEms19rPQ/TMHN0bCw2PjLnU3p//FMJVhHFjBAbKpk3692Tc7A5p85UMpumH27v7WKGCyezTSWzOY7O8Rkbm89PnQ3wZpCOtT1CEE3AI57MxE7m4I89D5WRzDEOopLJpTMnn0MlE3WbtKMmdYqJ6nJ2Rrd2yPzsm2PgbXevqGQiLPDrtcddfHpe5bB1DK59V8+PJfqmHwOy9SMf+6V5hfWO7IbuKO967b2ZOV+yzv9SdbWjfWSS4pnjlx2jNc7qhu99oSI9qBgYKjuAypuwMkqoXBuqCgFVn3G2bLNVrkKXFaqmB1XtxKoDQ1XIoXoHWF0VqH4TVicOqkeJ1b2F6mtjdfyhZiGwpkSg5mewJouwZq6gptGw5vSwJhixZjuxpl6h5oGxJqWxZsgrqOn6CmvvAGsjA2tXpYLa4qmw9psqqM2vCmsnLpIO0PJiRioLgDZMX8DZvV1Rh7aSizFZgbOv/QbIJvsXXyDgH8LcMHRihxyvAAAAAElFTkSuQmCC"/>
              <h4 className="text-center">InnoMinds</h4>
            </div>
            <div className="col-md-8">
              <form className="form-signin">
                <input type="text" className="form-control mb-2" placeholder="Email" name="email" onChange={(event) => this.handleInut(event)}  required autoFocus/>
                <input type="password" className="form-control mb-2" name="password" onChange={(event) => this.handleInut(event)}  placeholder="Password" required/>
                <button className="btn btn-lg btn-primary btn-block mb-1" onClick={this.onSubmit.bind(this)} type="submit">Sign in</button>              
               
              </form>
            </div>
          </div>
        </div>
      </div>
     
    </div>
  </div>
</div>
         );
    }
}
 
function mapStateToProps(state){
    
    console.log(state);
       return{
           isLoggedIn:state.authReducer.is_logged_in
       };
   }
   
   function mapDispatchToProps(dispatch){
   
       return{
           authActions: bindActionCreators(authActions,dispatch),
       };
   }
   export default connect(mapStateToProps, mapDispatchToProps)(Login);
