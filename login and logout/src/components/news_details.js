import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as newsActions from '../store/actions/newsActions'


class NewsDetails extends Component {
    constructor(props){
        super(props);
        this.state={
            newsItem:{}
        }
    }
    componentWillMount(){
        this.props.newsActions.fetchNewsById(this.props.match.params.id);     
    }

    componentWillReceiveProps(newProps){
        debugger;
        console.log("will recievie"+newProps.newsItem);
        if(newProps.newsItem){
            this.setState({
                newsItem:newProps.newsItem
            })
        }
        console.log("porpsdf"+newProps.newsItem);
    }
    render() {
        console.log(this.state.newsItem.body);
        debugger;
        return (
           <div>
                 <p>{this.state.newsItem.body}</p>
                 <p>{this.state.newsItem.timestamp}</p>
                 <p>{this.state.newsItem.tags}</p>
                 <p>{this.state.newsItem.comments}</p>
                 
           </div>
        );
    }
}

function mapStateToProps(state){
    
 console.log(state);
    return{
        newsItem : state.newsReducer.newsItem,
    };
}

function mapDispatchToProps(dispatch){

    return{
        newsActions: bindActionCreators(newsActions,dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsDetails);