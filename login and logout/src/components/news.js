import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as newsActions from '../store/actions/newsActions'


class News extends Component {
    componentDidMount(){
        this.props.newsActions.fetchNews();
        debugger;
    }
    render() {
        return (
            <div>
                {
                    (this.props.newsItems)
                        ? <div>
                            {this.props.newsItems.map(item => (
                                <div key={item._id} class="container-fluid">

                                    <h2><a href={`/news/${item._id}`}>{item.heading}</a></h2>
                                    {/* <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{item.brief}</p>
                                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{item.body}</p>
                                    <p>{item.timestamp}</p>
                                    <p>{item.tags}</p>
                                    <p>{item.comments}</p>
                                    <p>{item.likes.likes}</p>
                                    <p>{item.likes.dislikes}</p> */}
                                </div>
                            ))}
                        </div>
                        : <div>
                            loading...
                        </div>
                }
            </div>
        );
    }
}

function mapStateToProps(state){
  
    return{
        newsItems : state.newsReducer.news,
    };
}

function mapDispatchToProps(dispatch){

    return{
        newsActions: bindActionCreators(newsActions,dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(News);